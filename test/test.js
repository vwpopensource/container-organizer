const _ = require("lodash");
const expect = require('expect.js');

const containerOrganizer = require("./../containerOrganizer");

const _items = [];
for (let i = 0; i <= 5; ++i) {
    let item = {
        _id: Math.random().toString(32).substring(2),
        weight: Math.floor(Math.random() * 10) || 0.3,
        amount: Math.floor(Math.random() * 5) || 1,
        _length: Math.floor(Math.random() * 105) || 16,
        width: Math.floor(Math.random() * 105) || 11,
        height: Math.floor(Math.random() * 105) || 2,
    };
    _items.push(item)
}


const _reducer = ({totalWeight, totalCubing}, e) => {

    if (!e) {
        return {totalWeight, totalCubing}
    }

    return ({
        totalWeight: totalWeight + e.weight,
        totalCubing: totalCubing + e.cubing
    })
};
let groups = {};
let totalLength = 0;

const container = containerOrganizer(_items);

describe("Container Format ", function () {
    it("Container need be Array", function () {
        expect(container).to.be.an(Array);
    });
});

container.map(item => {
    const result = item.box.reduce(_reducer, {totalCubing: 0, totalWeight: 0});
    describe(item.title, function () {
        it("Cubing shoud not be above than " + containerOrganizer.VOLUMETRIC_CUBING_LIMIT, () => {
            expect(result.totalCubing).to.not.be.above(containerOrganizer.VOLUMETRIC_CUBING_LIMIT);
        });
        it("Weight shoud not be above than " + containerOrganizer.WEIGHT_LIMIT, () => {
            expect(result.totalWeight).to.not.be.above(containerOrganizer.WEIGHT_LIMIT);
        });
    });
    totalLength += item.boxLength;
    groups = Object.assign({}, groups, _.groupBy(item.box, "parent"));
});

describe("ALL ITEMS", function () {
    it("check all original items  is inside the Boxes", function () {
        expect(Object.keys(groups).length === _items.length).to.be.ok();
    });
    it("check generated items is inside the Boxes", function () {
        expect(totalLength).to.not.be.above(containerOrganizer.ITEMS_TOTAL);
        expect(totalLength).to.not.be.below(containerOrganizer.ITEMS_TOTAL);
    })
});
