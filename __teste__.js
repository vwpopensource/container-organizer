const fs = require("fs");
const path = require("path");
const random = require('random-name');
const faker = require("faker");
const _ = require("lodash");
const containerOrganizer = require("./containerOrganizer");
const outFolder = path.join(__dirname, "test_out");
fs.existsSync(outFolder) || fs.mkdirSync(outFolder, 0o777);

const _items = [];
for (let i = 0; i < 5; ++i) {
    let item = {
        _id: Math.random().toString(32).substring(2),
        weight: Math.floor(Math.random() * 30) || 0.3,
        amount: Math.floor(Math.random() * 5) || 1,
        _length: Math.floor(Math.random() * 105) || 16,
        width: Math.floor(Math.random() * 105) || 11,
        height: Math.floor(Math.random() * 105) || 2,
        price: Number(faker.commerce.price().replace(/\D/ig, ''))
    };
    _items.push(item)
}

const container = containerOrganizer(_items);

random();

const owner = path.join(outFolder, random.first() + '_' + random.last()) + ".json";


const _reducer = ({totalWeight, totalCubing}, e) => {

    if (!e) {
        return {totalWeight, totalCubing}
    }

    return ({
        totalWeight: totalWeight + e.weight,
        totalCubing: totalCubing + e.cubing
    })
};
let groups = {};
let error = false;

container.map(item => {
    const result = item.box.reduce(_reducer, {totalCubing: 0, totalWeight: 0});
    if (result.totalCubing > containerOrganizer.VOLUMETRIC_CUBING_LIMIT) {
        error = true;
        console.error("ERROR IN " + item.title + " EXECDED VOLUMETRIC_CUBING_LIMIT (" + containerOrganizer.VOLUMETRIC_CUBING_LIMIT + ") ")
    } else if (result.totalWeight > containerOrganizer.WEIGHT_LIMIT) {
        error = true;
        console.error("ERROR IN " + item.title + " EXECDED WEIGHT_LIMIT (" + containerOrganizer.WEIGHT_LIMIT + ") ")
    } else {
        error = false;
        console.log(item.title + " STATUS OK. Items\n(TOTAL:" + item.boxLength + " CUBBING: " + result.totalCubing + " WEIGHT: " + result.totalWeight);
    }
    groups = Object.assign({}, groups, _.groupBy(item.box, "parent"));
});

if (Object.keys(groups).length !== _items.length) {
    console.error("ITEMS LENGTH ERROR");
    error = true;
}
if (error) {
    console.error("APPLICATION EXIT WITH ERROR");
    process.exit(1);
} else {
    fs.writeFileSync(
        owner,
        JSON.stringify({container, _items, date: new Date()}, null, 4)
    );
    console.log(`See resuts in: \n${owner}\nOK`);
    process.exit(0);

}

/*
JSON.stringify(cart.products.reduce((items, v)=>{
let item = Object.assign({}, v.variation.measures)
item._id = v.variation._id;
item.amount = v.itemAmount;
items.push(item);
return items;
}, []))
 */