# Container Organizer

O organizador de container tem como objetivo organizar um array de produtos
da melhor forma possível, dentro de caixas. 
O projeto segue os padrões da Empresa Brasileira de Correios e Telegrafos (Correios),
sendo esse padrões: 

- Largura: de 11cm até 105cm.
- Comprimento: de 16cm até 105cm.
- Altura: de 2cm até 105cm.

O modelo de produto esperado:

```javascript

const DefaultItem = {
    weight: 0.3,
    _length: 16,
    width: 11,
    height: 2
}

```

O resultado final 

```javascript
const boxes = [
    {
        title: "Box 0",
        box: Array<DefaultItem>,
        boxLength: 5
    }
]
```

Depois de clonar o projeto, rode:

```npm
npm install
```


```npm
npm test
```

Veja os resultados em `test_out`