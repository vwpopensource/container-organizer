const clone = require("clone");
const MAX_PRICE_VALUE = 300000;
const MIN_PRICE_VALUE = 1850;
const WEIGHT_LIMIT = 30;
const VOLUMETRIC_CUBING_LIMIT = 200;
const CUBING_FACTOR = 6000;
const DefaultDimens = {
    _length: 16,
    width: 11,
    height: 2,
    weight: 0.3,
};

const DefaultItem = Object.assign({}, DefaultDimens, {
    _id: Math.random().toString(32).substring(2),
    cubing: 29,
    parser(item) {
        const keys = Object.keys(DefaultDimens);
        keys.map(k => {
            if (!item[k]) {
                item[k] = 1;
            }
            item[k] = Number(item[k]) || 1;
        });

        // ALTURA * LARGURA * COMPRIMENTO / FATOR DE CUBAGEM
        // http://www.lojamestre.com/helpdesk/knowledgebase.php?article=339
        // Padrão seguido pelos Corrios do Brasil

        item.cubing = (item.height * item.width * item._length) / CUBING_FACTOR;
        return item;
    }
});

const formatMoney = (v) => {
    v = v + '';
    if (v.length === 1) {
        v = '00' + v
    }
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/(\d{2})$/, ".$1"); //Coloca a virgula
    v = v.replace(/(\d+)(\d{3},\d{2})$/g, "$1,$2"); //Coloca o primeiro ponto
    let qtdLoop = (v.length - 3) / 3; // loops
    let count = 0;
    while (qtdLoop > count) {
        count++;
        v = v.replace(/(\d+)(\d{3}.*)/, "$1$2"); //Coloca o resto dos pontos
    }
    v = v.replace(/^(0)(\d)/g, "$2"); //Coloca hífen entre o quarto e o quinto dígitos
    return v;
};

const boxParser = dimensions => {
    const keys = Object.keys(DefaultDimens);
    keys.map(k => {
        if ("weight" === k) {
            return false;
        }

        dimensions[k] = dimensions[k] || DefaultDimens[k];
        dimensions[k] = dimensions[k] >= DefaultDimens[k] ? dimensions[k] : DefaultDimens[k];

        return true;
    });
    return dimensions;
};

const _sortDesc = (by, a, b) => {
    if (!b) {
        return -1
    }

    if (a[by] === b[by]) {
        return 0;
    }

    return a[by] > b[by] ? -1 : 1
};
const _friendlyFilter = (needed, item, by) => item[by] <= needed;


const composeBox = (base, friendlyItems) => {
    let box = [base];
    const rest = [];
    if (friendlyItems.length > 1) {
        friendlyItems = friendlyItems.sort(_sortDesc.bind(null, "weight"));
        for (let i = 0; i < friendlyItems.length; ++i) {
            box.push(friendlyItems[i]);
            let totalWeight = box.reduce((totalW, item) => totalW + item.weight, 0);
            let totalCubing = box.reduce((totalC, item) => totalC + item.cubing, 0);
            if (box.length === 1) {
                continue
            }
            let last = null;
            if (
                totalWeight > WEIGHT_LIMIT
                || totalCubing > VOLUMETRIC_CUBING_LIMIT
            ) {
                last = box.pop();
                if (!rest.find(i => i._id === last._id)) {
                    rest.push(last);
                }
            }
        }
    }
    box = box.filter(i => i);
    return {box, rest};
};

/**
 *
 * @param {Array} items
 * @private
 * @return {Array<DefaultItem>}
 */
const _explodeAmount = items => {
    const _exploded = [];
    items.map(item => {
        item.amount = Number(item.amount);
        item.type = "original";
        item.parent = item._id;
        _exploded.push(item);
        if (item.amount > 1) {
            const amount = item.amount - 1;
            for (let i = 0; i < amount; ++i) {

                const _clone = clone(item);
                _clone._id += "_" + (i + 2);
                _clone.type = "clone_from_" + item._id;
                _clone.parent = item._id;
                _clone.amount = 1;
                _clone.price = item.price;
                _exploded.push(_clone);
            }
        }

    });
    return _exploded;
};

const boxReducer = ({cubing = 0, weight = 0}, item) => ({
    cubing: cubing + item.cubing,
    weight: (Number(weight) + Number(item.weight)).toFixed(2)
});

const getProperties = box => {

    const props = box.reduce(boxReducer, {cubing: 0, weight: 0, _length: 0, width: 0, height: 0});
    let corner = Math.cbrt(props.cubing).toFixed(2);
    corner = Number(corner);

    props.width = props.height = props._length = corner;
    return boxParser(props);
};

function generateBox(box, container) {
    const item = box.length > 1 ? box.sort((a, b) => a.price > b.price ? -1 : 1)[0] : box[0];
    let price = item.price > MAX_PRICE_VALUE ? MAX_PRICE_VALUE : item.price;
    price = price < MIN_PRICE_VALUE ? MIN_PRICE_VALUE : price;
    String(price).split(".")[0].replace(/\D/, '');
    price = formatMoney(price);
    return {
        title: `Box ${container.length + 1}`,
        box,
        boxLength: box.length,
        dimensions: getProperties(box),
        price
    }
}

/**
 * Função que recebe os items e organiza o container com caixas
 * retorna um array
 *
 * @param container
 * @param items
 * @param isParsed
 * @return {Array}
 */

const containerOrganizer = ({container = [], items = []}) => {
    if (!items.length) {
        return container;
    }

    let _items = items.map(DefaultItem.parser).sort(_sortDesc.bind(null, "cubing"));

    const item = _items.shift();

    const cubingRest = VOLUMETRIC_CUBING_LIMIT - item.cubing;

    const friendlyItems = _items.length > 1 ? _items.filter(item => _friendlyFilter(cubingRest, item, "cubing")) : [item];

    const containerBox = composeBox(item, friendlyItems);
    container.push(generateBox(containerBox.box, container));

    items = items.filter(__item => containerBox.box.map(i => i._id).indexOf(__item._id) === -1);

    return containerOrganizer({container, items});
};

/**
 * Função responsável por fazer o parser dos items
 * e prepará-los para a função containerOrganizer
 * @param items
 * @return {Array}
 */

const parserItems = items => {
    const _items = _explodeAmount(items);
    parserItems.ITEMS_TOTAL = _items.length;
    return containerOrganizer({items: _items});
};


parserItems.WEIGHT_LIMIT = WEIGHT_LIMIT;
parserItems.VOLUMETRIC_CUBING_LIMIT = VOLUMETRIC_CUBING_LIMIT;
parserItems.CUBING_FACTOR = CUBING_FACTOR;


module.exports = exports = parserItems;